/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.7.11-log : Database - nst
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nst` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `nst`;

/*Table structure for table `korisnik` */

CREATE TABLE `korisnik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `korisnik` */

insert  into `korisnik`(`id`,`username`) values (1,'test');
insert  into `korisnik`(`id`,`username`) values (2,'Zookey');
insert  into `korisnik`(`id`,`username`) values (3,'Doboj');

/*Table structure for table `tim` */

CREATE TABLE `tim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `opis` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tim` */

insert  into `tim`(`id`,`name`,`opis`) values (1,'Kabinet','Vrh softver tim');
insert  into `tim`(`id`,`name`,`opis`) values (3,'dada','da');
insert  into `tim`(`id`,`name`,`opis`) values (4,'SILAB','dodo');
insert  into `tim`(`id`,`name`,`opis`) values (5,'dada1','da1');
insert  into `tim`(`id`,`name`,`opis`) values (6,'12','12');

/*Table structure for table `tim_korisnik` */

CREATE TABLE `tim_korisnik` (
  `tim_id` int(11) DEFAULT NULL,
  `korisnik_id` int(11) DEFAULT NULL,
  KEY `fk_korisnik` (`korisnik_id`),
  KEY `fk_tim` (`tim_id`),
  CONSTRAINT `fk_korisnik` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_tim` FOREIGN KEY (`tim_id`) REFERENCES `tim` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tim_korisnik` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
