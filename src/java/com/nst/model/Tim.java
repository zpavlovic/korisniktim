/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nst.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Zoran
 */
@Entity
@Table(name = "tim")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tim.findAll", query = "SELECT t FROM Tim t"),
    @NamedQuery(name = "Tim.findById", query = "SELECT t FROM Tim t WHERE t.id = :id")})
public class Tim implements Serializable, AbstractModelObject {

    @OneToMany(mappedBy = "timId")
    private Collection<Projekat> projekatCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "name")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "opis")
    private String opis;
      @JoinTable(name = "tim_korisnik", joinColumns = {
        @JoinColumn(name = "tim_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "korisnik_id", referencedColumnName = "id")})
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<Korisnik> korisnikCollection;

    public Tim() {
    }

    public Tim(Integer id) {
        this.id = id;
    }

    public Tim(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @XmlTransient
    public Collection<Korisnik> getKorisnikCollection() {
        return korisnikCollection;
    }

    public void setKorisnikCollection(Collection<Korisnik> korisnikCollection) {
        this.korisnikCollection = korisnikCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tim)) {
            return false;
        }
        Tim other = (Tim) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.nst.model.Tim[ id=" + id + " ]";
    }

   @Override
    public String getTableName() {
        return "Tim";
    }

    @Override
    public String getShortTableName() {
        return "t";
    }

    @Override
    public int getPK() {
       return id;
    }

    @XmlTransient
    public Collection<Projekat> getProjekatCollection() {
        return projekatCollection;
    }

    public void setProjekatCollection(Collection<Projekat> projekatCollection) {
        this.projekatCollection = projekatCollection;
    }
    
}
