/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nst.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Zoran
 */
@Entity
@Table(name = "projekat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Projekat.findAll", query = "SELECT p FROM Projekat p"),
    @NamedQuery(name = "Projekat.findById", query = "SELECT p FROM Projekat p WHERE p.id = :id")})
public class Projekat implements Serializable, AbstractModelObject {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "name")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "opis")
    private String opis;
    @JoinColumn(name = "tim_id", referencedColumnName = "id")
    @ManyToOne
    private Tim timId;

    public Projekat() {
    }

    public Projekat(Integer id) {
        this.id = id;
    }

    public Projekat(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Tim getTimId() {
        return timId;
    }

    public void setTimId(Tim timId) {
        this.timId = timId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Projekat)) {
            return false;
        }
        Projekat other = (Projekat) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.nst.model.Projekat[ id=" + id + " ]";
    }

    @Override
    public String getTableName() {
        return "Projekat";
    }

    @Override
    public String getShortTableName() {
        return "p";
    }

    @Override
    public int getPK() {
        return id;
    }
    
}
