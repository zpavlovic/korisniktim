/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nst.controler;

import com.nst.db.DatabaseBroker;
import com.nst.model.AbstractModelObject;
import com.nst.model.Korisnik;
import com.nst.model.Tim;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Zoran
 */
@ManagedBean
@SessionScoped
public class KorisnikControler {

    @EJB
    DatabaseBroker databaseBroker;

    public String username;
    public Korisnik korisnik;
    public List<Tim> selectedTim;
    public List<Tim> timList = new ArrayList<Tim>();

    @PostConstruct
    public void init() {
        List<AbstractModelObject> currentList = databaseBroker.load(new Tim());
        if (currentList.size() > 0) {
            for (int i = 0; i < currentList.size(); i++) {
                Tim t = (Tim) currentList.get(i);
                timList.add(t);
            }
        }
    }

    public void saveUser() {
        korisnik = new Korisnik();
        korisnik.setTimCollection(selectedTim);
        korisnik.setUsername(username);
        databaseBroker.save(korisnik);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Tim> getSelectedTim() {
        return selectedTim;
    }

    public void setSelectedTim(List<Tim> selectedTim) {
        this.selectedTim = selectedTim;
    }

    public List<Tim> getTimList() {
        return timList;
    }

    public void setTimList(List<Tim> timList) {
        this.timList = timList;
    }

}
