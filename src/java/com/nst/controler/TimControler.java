/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nst.controler;

import com.nst.db.DatabaseBroker;
import com.nst.model.AbstractModelObject;
import com.nst.model.Korisnik;
import com.nst.model.Tim;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Zoran
 */

@ManagedBean
@ApplicationScoped
public class TimControler {
    
    @EJB
    DatabaseBroker databaseBroker;
    
    public Tim tim;
    public String name;
    public String desc;
    public List<Korisnik> selectUsers;   
    public List<Korisnik> usersList = new ArrayList<Korisnik>();
    
    
    @PostConstruct
    public void init() {
        List<AbstractModelObject> currentList = databaseBroker.load(new Korisnik()); 
        if(currentList.size() > 0){
            for(int i=0;i<currentList.size();i++){
                Korisnik t = (Korisnik) currentList.get(i);
                usersList.add(t);
            }
        }
      
    }

    public void save(){
        tim = new Tim();
        tim.setName(name);
        tim.setOpis(desc);        
        tim.setKorisnikCollection(selectUsers);       
        databaseBroker.save(tim);
       
    }

    public Tim getTim() {
        return tim;
    }

    public void setTim(Tim tim) {
        this.tim = tim;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<Korisnik> getSelectUsers() {
        return selectUsers;
    }

    public void setSelectUsers(List<Korisnik> selectUsers) {
        this.selectUsers = selectUsers;
    }

    public List<Korisnik> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Korisnik> usersList) {
        this.usersList = usersList;
    }
 
    
    
}
