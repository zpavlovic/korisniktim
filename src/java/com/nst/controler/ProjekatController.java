/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nst.controler;

import com.nst.db.DatabaseBroker;
import com.nst.model.AbstractModelObject;
import com.nst.model.Projekat;
import com.nst.model.Tim;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Zoran
 */
@ManagedBean
@RequestScoped
public class ProjekatController {

    @EJB
    DatabaseBroker databaseBroker;

    public Projekat projekat;
    public Tim tim;
    public List<Tim> timList = new ArrayList<Tim>();
    public String name, desc;

    @PostConstruct
    public void init() {
        List<AbstractModelObject> currentList = databaseBroker.load(new Tim());
        if (currentList.size() > 0) {
            for (int i = 0; i < currentList.size(); i++) {
                Tim t = (Tim) currentList.get(i);
                timList.add(t);
            }
        }
    }

    public void save() {
        projekat = new Projekat();
        projekat.setName(name);
        projekat.setOpis(desc);
        projekat.setTimId(tim);
        databaseBroker.save(projekat);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Tim getTim() {
        return tim;
    }

    public void setTim(Tim tim) {
        this.tim = tim;
    }

    public List<Tim> getTimList() {
        return timList;
    }

    public void setTimList(List<Tim> timList) {
        this.timList = timList;
    }

}
