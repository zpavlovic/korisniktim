package com.nst.db;


import com.nst.model.AbstractModelObject;
import com.nst.model.Tim;
import java.util.List;
import javax.ejb.Local;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Zoran
 */

@Local
public interface DatabaseBroker {
    
    public void save(AbstractModelObject amo);
    public List<AbstractModelObject> load(AbstractModelObject amo);
    public List<Tim> vratiTimoveZaKorisnika(String id);
}
