/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nst.db;

import com.nst.model.AbstractModelObject;
import com.nst.model.Tim;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Zoran
 */

@Stateless
public class DatabaseBrokerImpl implements DatabaseBroker{
    
    @PersistenceContext
    EntityManager em;

    @Override
    public void save(AbstractModelObject amo) {
        try{
            em.merge(amo);
        } catch (Exception e){
            System.err.println(e.getLocalizedMessage());
        }     
    }

    @Override
    public List<AbstractModelObject> load(AbstractModelObject amo) {
        try{
            String query = "SELECT " + amo.getShortTableName() + " FROM " + amo.getTableName() + " " + amo.getShortTableName();
            Query q = em.createQuery(query);
            return q.getResultList();
        } catch (Exception e){
            e.getLocalizedMessage();
            return null;
        }
    }

    @Override
    public List<Tim> vratiTimoveZaKorisnika(String id) {
            try{
            String query = "SELECT t FROM Tim t INNER JOIN t.id id WHERE id = :id";
            Query q = em.createQuery(query);
            return q.getResultList();
        } catch (Exception e){
            e.getLocalizedMessage();
            return null;
        }
    }
    
    
}
