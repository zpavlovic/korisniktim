
import com.nst.db.DatabaseBroker;
import com.nst.model.AbstractModelObject;
import com.nst.model.Korisnik;
import com.nst.model.Tim;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Zoran
 */
@ManagedBean
@RequestScoped
public class KorisnikConverter implements Converter {

    @EJB
    DatabaseBroker databaseBroker;
    List<AbstractModelObject> list;
    List<Korisnik> korisnikList = new ArrayList<Korisnik>();
    List<Tim> timList = new ArrayList<Tim>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.isEmpty()) {
            return null;
        }

        System.out.println("NUMBER: " + submittedValue);
        list = databaseBroker.load(new Korisnik());
        System.out.println("NUMBER size: " + String.valueOf(list.size()));

        int number = Integer.parseInt(submittedValue);

        for (int i = 0; i < list.size(); i++) {
            Korisnik k = (Korisnik) list.get(i);
            korisnikList.add(k);
        }

        try {

            for (int i = 0; i < korisnikList.size(); i++) {
                if (korisnikList.get(i).getId() == number) {
                    System.out.println("NUMBER: " + String.valueOf(i) + " JEST jednak IDu iz baze!");
                    return korisnikList.get(i);
                } else {
                    System.out.println("NUMBER: " + String.valueOf(i) + " nije jednak IDu iz baze...");
                }
            }

        } catch (Exception e) {
            e.getLocalizedMessage();
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
           if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Korisnik) value).getId());
        }
    }

}
